package com.hendisantika.springbootflywaypostgresdocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootFlywayPostgresDockerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootFlywayPostgresDockerApplication.class, args);
    }

}
