package com.hendisantika.springbootflywaypostgresdocker.repository;

import com.hendisantika.springbootflywaypostgresdocker.domain.Vehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-flyway-postgres-docker
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-03-19
 * Time: 08:57
 */
@RepositoryRestResource
public interface VehicleRepository extends CrudRepository<Vehicle, Long> {
}