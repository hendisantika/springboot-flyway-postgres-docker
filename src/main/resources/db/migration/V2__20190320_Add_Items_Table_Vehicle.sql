INSERT INTO vehicle (manufacturer, model_name, production_year) VALUES
('Honda', 'Civic', 2003),
('Honda', 'Jazz', 2013),
('Honda', 'Freed', 2014),
('Nissan', 'Leaf', 2015),
('Nissan', 'X-trail', 2016),
('Suzuki', 'Swift', 2008),
('Suzuki', 'Ertiga', 2017),
('Mitsubishi', 'Expander', 2017),
('Mitsubishi', 'Pajero', 2017),
('Mazda', '3', 2010);